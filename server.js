const path = require('path');
const gateway = require('express-gateway');

//==================================================

const PORT = process.env.PORT || 1234;
const express = require('express');
var app = express();

var flash = require('connect-flash');

app.use(require('express-session')({ cookie: { maxAge: 60000 }, secret:'keyboard cat', saveUninitialized: true, resave: true,}));
app.use(require('cookie-parser')('keyboard cat'));
app.use(flash());
app.use(
    require('body-parser').urlencoded({
        extended:true
    })
);

app.get('/', (req, res) => {
    res.send('Welcome to the Homepage.')
})

//==================================================

const passport = require('passport');

app.get('/success', (req, res) => {
    res.send(`Welcome ${req.query.username}`)
});

passport.serializeUser(function (user, cb) {
    cb(null, user.id)
});

passport.deserializeUser(function (id, cb) {
    User.findById(id, function (err, user) {
        cb(err, user);
    })
})

//==================================================

const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://admin:admin@testdb-60cyz.mongodb.net/test2?retryWrites=true&w=majority', {useNewUrlParser:true})

mongoose.connection.on('error', function () {
    console.error("Failed to connect to database");
    process.exit(1);
});
mongoose.connection.once('open', function () {
    console.log("Connected to database");
});

const userSchema = new mongoose.Schema({
  name: String,
  user: String,
  hash: String,
  salt: String,
})

const User = mongoose.model('user', userSchema);

//==================================================

// const testUserSalt = bcrypt.genSaltSync();
// const testUserHash = bcrypt.hashSync("password", testUserSalt).split('.')[1];
// const newUser = new User({
//   name:'John Doe',
//   user:'johndoe',
//   hash:testUserHash,
//   salt:testUserSalt.toString(),
// })

// newUser.save((idk) => {
//   console.log(idk)
// });

//==================================================

const LocalStrategy = require('passport-local').Strategy;


passport.use(new LocalStrategy({ usernameField: 'username', passwordField: 'password', session:false}, (username, password, done) => {
  return done(null, {id: 1, name:'johndoe', })
}))

app.use(passport.initialize());
app.use(passport.session());

app.get('/testsetcookie', (req, res) => {
  res.cookie('testkey', 'testval');
  res.send('idkwhat')
});

app.get('/error', (req, res) => {
  res.send('asdasjds');
});

app.post('/users/register', (req, res) => {
  if (!req.body.name || !req.body.user || !req.body.pass)
  {
    res.send("All fields is not filled.");
    return;
  }

  const newSalt = bcrypt.genSaltSync();
  const newHash = bcrypt.hashSync(req.body.pass, newSalt).split('.')[1]
  const newUser = new User({
    name:req.body.name,
    user:req.body.user,
    salt:newSalt,
    hash:newHash,
  })

  newUser.save();
  res.send('Success. Now try login');
})

app.post('/users/login', passport.authenticate('local', {
  successRedirect: '/',
  failureRedirect: '/error',
  failureFlash: true,
}));

app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});

gateway()
  .load(path.join(__dirname, 'config'))
  .run();